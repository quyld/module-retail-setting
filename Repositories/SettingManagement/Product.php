<?php
/**
 * Created by mr.vjcspy@gmail.com - khoild@smartosc.com.
 * Date: 12/01/2017
 * Time: 11:28
 */

namespace SM\Setting\Repositories\SettingManagement;


class Product extends AbstractSetting {

    /**
     * @var string
     */
    protected $CODE = 'product';
    /**
     * @var \SM\CustomSale\Helper\Data
     */
    protected $customSaleHelper;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    /**
     * Product constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \SM\CustomSale\Helper\Data                         $customSaleHelper
     * @param \Magento\Catalog\Model\ProductFactory              $productFactory
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \SM\CustomSale\Helper\Data $customSaleHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->customSaleHelper = $customSaleHelper;
        $this->productFactory   = $productFactory;
        parent::__construct($scopeConfig);
    }

    /**
     * @return array
     */
    public function build() {
        return [
            'custom_sale_product_id' => $this->customSaleHelper->getCustomSaleId(),
            'product_attributes'     => $this->getProductAttributes()
        ];
    }

    /**
     * @return array
     */
    protected function getProductAttributes() {
        $attributes     = $this->getProductModel()->getAttributes();
        $attributeArray = [];

        foreach ($attributes as $attribute) {
            $attributeArray[] = [
                'label' => $attribute->getFrontend()->getLabel(),
                'value' => $attribute->getAttributeCode()
            ];
        }

        return $attributeArray;
    }

    /**
     * @return  \Magento\Catalog\Model\Product
     */
    protected function getProductModel() {
        return $this->productFactory->create();
    }
}